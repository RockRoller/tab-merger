# tab-merger

A small browser (Firefox) extension I wrote to make it easier to manage my tabs. Should work for anyone not just my browser, but no guarantees!

# Features

Host always refers to subdomain + domain + TLD, no pathname or protocal, e.g. `www.reddit.com`.

-   show how many tabs are open
-   show how many of those tabs are pinned
-   show how many tabs are open, per host
-   hide all tabs of a host
    - hiding refers to setting the `hidden` property of a tab to true, which in Firefox puts them into the overflow menu at the end of the tabslist
-   unhide all tabs of a host
    -   holding `CTRL` while clicking this will open all non-pinned ones in a new window
-   close all tabs of a host
    -   holding `CTRL` while clicking this will skip the confirmation message
-   Add 2 new commands, which go to the next/previous tab that has the same host. This is not limited to the active window.
    -   `CTRL + Right Arrow`
    -   `CTRL + Left Arrow`
