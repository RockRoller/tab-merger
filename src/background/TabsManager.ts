import { hostCount } from "./types"

/**
 * Handles all tab manipulation
 * accessible as default export
 */
class TabsManager {
    /**
     * hides all non-pinned tabs that match the given host
     * host = e.g. www.reddit.com
     * path and protocol are *, DO NOT PROVIDE
     * @param host e.g. www.reddit.com
     */
    async hideHost(host: string) {
        browser.tabs.hide((await this.getTabsByHost(host)).map((tab) => tab.id!))
    }

    /**
     * unhides all tabs that match the given host
     * host = e.g. www.reddit.com
     * path and protocol are *, DO NOT PROVIDE
     * @param host e.g. www.reddit.com
     */
    async showHost(host: string) {
        browser.tabs.show((await this.getTabsByHost(host, true)).map((tab) => tab.id!))
    }

    /**
     * opens all non-pinned tabs that match the given host in a new window
     * host = e.g. www.reddit.com
     * path and protocol are *, DO NOT PROVIDE
     * @param host e.g. www.reddit.com
     */
    async openHostInNewWindow(host: string) {
        const tabIds = (await this.getTabsByHost(host)).map((tab) => tab.id!)
        // open with first tab and unshift that one, else the window gets created with a new tab
        const window = await browser.windows.create({
            focused: true,
            state: "maximized",
            tabId: tabIds[0],
        })
        tabIds.shift()
        browser.tabs.move(tabIds, {
            windowId: window.id,
            index: -1,
        })
    }

    /**
     * closes all non-pinned tabs that match the given host
     * host = e.g. www.reddit.com
     * path and protocol are *, DO NOT PROVIDE
     * @param host e.g. www.reddit.com
     */
    async closeHost(host: string) {
        browser.tabs.remove((await this.getTabsByHost(host)).map((tab) => tab.id!))
    }

    /**
     * get all tabs that match the given host
     * by default pinned tabs are not included, @see includePinned
     * host = e.g. www.reddit.com
     * path and protocol are *, DO NOT PROVIDE
     * @param host e.g. www.reddit.com
     * @param includePinned should pinned tabs be included in the return
     */
    async getTabsByHost(host: string, includePinned = false) {
        if (includePinned) {
            return browser.tabs.query({ url: "*://" + host + "/*" })
        } else {
            return browser.tabs.query({ url: "*://" + host + "/*", pinned: false })
        }
    }

    /**
     * get the count of total tabs and pinned tabs
     * @returns count of total tabs and pinned tabs
     */
    async getCount() {
        return {
            tabs: (await browser.tabs.query({})).length,
            pinned: (await browser.tabs.query({ pinned: true })).length,
        }
    }

    /**
     * focuses the next/prev tab with the same host
     * @param direction -1 = prev; 1 = next
     */
    async navigateTabs(direction: -1 | 1) {
        const activeTab = await this.getFocusedTab()
        const tabs = await this.getTabsByHost(new URL(activeTab.url!).host)

        if (tabs.length > 1) {
            const activeIndex = tabs.findIndex((tab) => tab.id! == activeTab.id!)
            if (direction == 1) {
                activeIndex == tabs.length - 1 ? this.focusTab(tabs[0]) : this.focusTab(tabs[activeIndex + 1])
            } else {
                activeIndex == 0 ? this.focusTab(tabs[tabs.length - 1]) : this.focusTab(tabs[activeIndex - 1])
            }
        }
    }

    /**
     * focus the given tab and window
     * @param tab
     * @param window
     */
    private async focusTab(tab: browser.tabs.Tab) {
        await browser.tabs.update(tab.id!, { active: true })
        await browser.windows.update(tab.windowId!, { focused: true })
    }

    /**
     * gets the currently focused tab
     * focused tab = active in window + window is the current one
     * @returns currently focused tab
     */
    private async getFocusedTab() {
        return (await browser.tabs.query({ active: true, currentWindow: true }))[0]
    }

    /**
     * gets the count of tabs and how many of those are hidden per host
     * @returns hostCount[] @see hostCount
     */
    async getCountsPerHost(): Promise<hostCount[]> {
        const tabsByHost = new Map<string, browser.tabs.Tab[]>()

        ;(await browser.tabs.query({})).forEach((tab) => {
            const host = new URL(tab.url!).host

            const list = tabsByHost.get(host)
            if (list) {
                list.push(tab)
            } else {
                tabsByHost.set(host, [tab])
            }
        })

        return Array.from(tabsByHost.entries())
            .map((entry) => {
                return {
                    countTotal: entry[1].length,
                    name: entry[0],
                    countHidden: entry[1].filter((tab) => tab.hidden == true).length,
                }
            })
            .sort((a, b) => {
                if (a.name.toLowerCase() < b.name.toLowerCase()) return -1
                if (a.name.toLowerCase() > b.name.toLowerCase()) return 1
                return 0
            })
    }

    /**
     * similar to getCountsPerHost but for a single host
     * @param host e.g. www.reddit.com
     */
    async getCountByHost(host: string): Promise<hostCount> {
        const tabs = await this.getTabsByHost(host)
        return {
            name: host,
            countHidden: tabs.filter((tab) => tab.hidden == true).length,
            countTotal: tabs.length,
        }
    }
}

export default new TabsManager()
