import { IconType } from "../../typescript-lib/src/constants/types"
import { createElement, createIcon } from "../../typescript-lib/src/shared/dom"
import TabsManager from "./TabsManager"
import { hostCount } from "./types"

/**
 * renders the popup for the extension.
 * @param window Window object of the popup
 */
export async function renderPopup(window: Window) {
    const body = createElement("body", { className: "popup" })

    const count = await TabsManager.getCount()
    body.append(createLabeledRow("tabs", "", count.tabs))
    body.append(createLabeledRow("pinned tabs", "", count.pinned))
    body.append(createSeperator())
    ;(await TabsManager.getCountsPerHost()).forEach((entry) => body.append(createSiteListRow(entry, window)))

    window.document.body.replaceWith(body)
}

/**
 * creates a popup row for displaying a host with tab stats and control options
 * @param entry data for this row, @see hostCount
 * @param window Window object of the popup, needed for close confirmation
 * @returns element
 */
function createSiteListRow(entry: hostCount, window: Window) {
    let element: HTMLElement
    const showButton = createRowButton("show")
    showButton.addEventListener("click", async (e) => {
        await TabsManager.showHost(entry.name)
        if (e.ctrlKey) {
            await TabsManager.openHostInNewWindow(entry.name)
        }
        element.replaceWith(createSiteListRow(await TabsManager.getCountByHost(entry.name), window))
    })

    const hideButton = createRowButton("hide")
    hideButton.addEventListener("click", async () => {
        await TabsManager.hideHost(entry.name)
        element.replaceWith(createSiteListRow(await TabsManager.getCountByHost(entry.name), window))
    })

    const deleteButton = createRowButton("delete")
    deleteButton.addEventListener("click", (e) => {
        if (e.ctrlKey) {
            TabsManager.closeHost(entry.name)
            element.remove()
        } else if (window.confirm("Are you sure?")) {
            TabsManager.closeHost(entry.name)
            element.remove()
        }
    })

    // special handing for new tab as it produces an illegal pattern of */// which the buttons do not work on
    if (entry.name == "" ? true : false) {
        element = createLabeledRow("new tab", entry.countHidden == 0 ? "" : entry.countHidden, entry.countTotal, [])
    } else {
        element = createLabeledRow(entry.name, entry.countHidden == 0 ? "" : entry.countHidden, entry.countTotal, [showButton, hideButton, deleteButton])
    }
    return element
}

/**
 * creates a seperator.
 * Pretty overkill currently, but written with expansion in mind to make it easier to change seperators troughout the project.
 * @returns element
 */
function createSeperator() {
    return createElement("hr")
}

/**
 * creates a row of the popup
 * made up of the label on the left and either a string/number on the right or a html element on the right
 * @param label label on the left side
 * @param value1 first string/number to display, has muted text colour
 * @param value2 first string/number to display
 * @param elements optional, additional elements to be added onto the right half
 * @returns element
 */
function createLabeledRow(label: string, value1: string | number, value2: string | number, elements: HTMLElement[] = []) {
    return createElement("div", {
        className: "popup-row",
        children: [
            createElement("span", {
                className: "popup-row__label",
                attributes: {
                    innerText: label,
                },
            }),
            createElement("div", {
                className: "popup-row__options",
                children: [createPopupValue(value1, "hidden"), createPopupValue(value2), ...elements],
            }),
        ],
    })
}

/**
 * creates a value element for the right side of a popup row
 * @param value value to display, number or string
 * @param modifier optional modifier for classname
 * @returns element
 */
function createPopupValue(value: string | number, modifier: string = "") {
    return createElement("span", {
        className: `popup-row__value ${modifier == "" ? "" : "popup-row__value--" + modifier}`,
        attributes: {
            innerText: value.toString(),
        },
    })
}

/**
 * creates a button for createLabeledRow()
 * @param onClick function to trigger on click event
 * @param type what type of button, used to determine the icon and as title attr
 * @returns element
 */
function createRowButton(type: "show" | "hide" | "delete") {
    const iconMap = {
        show: "externalLink",
        hide: "collection",
        delete: "trash",
    }

    return createElement("button", {
        attributes: {
            title: type,
        },
        children: [createIcon(iconMap[type] as IconType)],
        className: "popup-row__button",
    })
}
