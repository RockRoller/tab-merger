/**
 * various counts for one host
 */
export type hostCount = {
    /**
     * name of the host, e.g. www.reddit.com
     */
    name: string
    /**
     * total count of tabs
     */
    countTotal: number
    /**
     * how many tabs from countTotal have hidden == true
     */
    countHidden: number
}
