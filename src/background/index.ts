import { renderPopup } from "./render"
import TabsManager from "./TabsManager"

/**
 * entry point of background script
 */
function main() {
    browser.runtime.onMessage.addListener((message) => {
        if (message == "render") {
            browser.extension.getViews({ type: "popup" }).forEach((popup) => {
                renderPopup(popup)
            })
        }
    })

    browser.commands.onCommand.addListener((command) => {
        if (command == "next-tab-of-type") {
            TabsManager.navigateTabs(1)
        }
        if (command == "prev-tab-of-type") {
            TabsManager.navigateTabs(-1)
        }
    })
}

main()
