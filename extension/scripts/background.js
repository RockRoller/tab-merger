'use strict';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

const arrow = `<svg viewBox="0 0 24 24" fill="none"><path d="M9 5L16 12L9 19" fill="#1c1719" stroke="#1c1719" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>`;
const at = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M14.243 5.757a6 6 0 10-.986 9.284 1 1 0 111.087 1.678A8 8 0 1118 10a3 3 0 01-4.8 2.401A4 4 0 1114 10a1 1 0 102 0c0-1.537-.586-3.07-1.757-4.243zM12 10a2 2 0 10-4 0 2 2 0 004 0z" clip-rule="evenodd"></path></svg>`;
const avatar = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>`;
const bookmark = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z"></path></svg>`;
const bookmarkFilled = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"><path d="M5 4a2 2 0 012-2h6a2 2 0 012 2v14l-5-2.5L5 18V4z"></path></svg>`;
const change = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7h12m0 0l-4-4m4 4l-4 4m0 6H4m0 0l4 4m-4-4l4-4"></path></svg>`;
const chat = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"></path></svg>`;
const close = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path></svg>`;
const collection = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10"></path></svg>`;
const copy = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 16H6a2 2 0 01-2-2V6a2 2 0 012-2h8a2 2 0 012 2v2m-6 12h8a2 2 0 002-2v-8a2 2 0 00-2-2h-8a2 2 0 00-2 2v8a2 2 0 002 2z"></path></svg>`;
const documentAdd = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path></svg>`;
const documentFilled = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" ><path fill-rule="evenodd" d="M4 4a2 2 0 012-2h4.586A2 2 0 0112 2.586L15.414 6A2 2 0 0116 7.414V16a2 2 0 01-2 2H6a2 2 0 01-2-2V4z" clip-rule="evenodd"></path></svg>`;
const download = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"></path></svg>`;
const externalLink = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path></svg>`;
const image = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg>`;
const message = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"></path></svg>`;
const refresh = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"></path></svg>`;
const search = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>`;
const setting = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>`;
const trash = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>`;
const warning = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z" clip-rule="evenodd"></path></svg>`;
const chevronDown = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path></svg>`;
const edit = `<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" ><path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z"></path><path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd"></path></svg>`;
const upload = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12"></path></svg>`;
const pause = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 9v6m4-6v6m7-3a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>`;
const play = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14.752 11.168l-3.197-2.132A1 1 0 0010 9.87v4.263a1 1 0 001.555.832l3.197-2.132a1 1 0 000-1.664z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>`;
const info = `<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>`;

var icons = /*#__PURE__*/Object.freeze({
    __proto__: null,
    arrow: arrow,
    at: at,
    avatar: avatar,
    bookmark: bookmark,
    bookmarkFilled: bookmarkFilled,
    change: change,
    chat: chat,
    close: close,
    collection: collection,
    copy: copy,
    documentAdd: documentAdd,
    documentFilled: documentFilled,
    download: download,
    externalLink: externalLink,
    image: image,
    message: message,
    refresh: refresh,
    search: search,
    setting: setting,
    trash: trash,
    warning: warning,
    chevronDown: chevronDown,
    edit: edit,
    upload: upload,
    pause: pause,
    play: play,
    info: info
});

function createElementFromHTMLString(html) {
    return createElement("div", {
        attributes: {
            innerHTML: html,
        },
    }).firstChild;
}
function createElement(tagName, options = {}) {
    const { attributes = {}, style = {}, className = "", children = [], events = [] } = options;
    const element = document.createElement(tagName);
    Object.assign(element, Object.assign(Object.assign({}, attributes), { className: `rr-lib ${className}` }));
    Object.assign(element.style, style);
    element.append(...children);
    events.forEach((event) => element.addEventListener(event.type, event.handler));
    return element;
}
function createIcon(name, options = {}) {
    const { size = 16 } = options, rest = __rest(options, ["size"]);
    const element = createElementFromHTMLString(icons[name]);
    Object.assign(element.style, Object.assign({ width: `${size}px`, height: `${size}px` }, rest));
    return element;
}

class TabsManager {
    hideHost(host) {
        return __awaiter(this, void 0, void 0, function* () {
            browser.tabs.hide((yield this.getTabsByHost(host)).map((tab) => tab.id));
        });
    }
    showHost(host) {
        return __awaiter(this, void 0, void 0, function* () {
            browser.tabs.show((yield this.getTabsByHost(host, true)).map((tab) => tab.id));
        });
    }
    openHostInNewWindow(host) {
        return __awaiter(this, void 0, void 0, function* () {
            const tabIds = (yield this.getTabsByHost(host)).map((tab) => tab.id);
            const window = yield browser.windows.create({
                focused: true,
                state: "maximized",
                tabId: tabIds[0],
            });
            tabIds.shift();
            browser.tabs.move(tabIds, {
                windowId: window.id,
                index: -1,
            });
        });
    }
    closeHost(host) {
        return __awaiter(this, void 0, void 0, function* () {
            browser.tabs.remove((yield this.getTabsByHost(host)).map((tab) => tab.id));
        });
    }
    getTabsByHost(host, includePinned = false) {
        return __awaiter(this, void 0, void 0, function* () {
            if (includePinned) {
                return browser.tabs.query({ url: "*://" + host + "/*" });
            }
            else {
                return browser.tabs.query({ url: "*://" + host + "/*", pinned: false });
            }
        });
    }
    getCount() {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                tabs: (yield browser.tabs.query({})).length,
                pinned: (yield browser.tabs.query({ pinned: true })).length,
            };
        });
    }
    navigateTabs(direction) {
        return __awaiter(this, void 0, void 0, function* () {
            const activeTab = yield this.getFocusedTab();
            const tabs = yield this.getTabsByHost(new URL(activeTab.url).host);
            if (tabs.length > 1) {
                const activeIndex = tabs.findIndex((tab) => tab.id == activeTab.id);
                if (direction == 1) {
                    activeIndex == tabs.length - 1 ? this.focusTab(tabs[0]) : this.focusTab(tabs[activeIndex + 1]);
                }
                else {
                    activeIndex == 0 ? this.focusTab(tabs[tabs.length - 1]) : this.focusTab(tabs[activeIndex - 1]);
                }
            }
        });
    }
    focusTab(tab) {
        return __awaiter(this, void 0, void 0, function* () {
            yield browser.tabs.update(tab.id, { active: true });
            yield browser.windows.update(tab.windowId, { focused: true });
        });
    }
    getFocusedTab() {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield browser.tabs.query({ active: true, currentWindow: true }))[0];
        });
    }
    getCountsPerHost() {
        return __awaiter(this, void 0, void 0, function* () {
            const tabsByHost = new Map();
            (yield browser.tabs.query({})).forEach((tab) => {
                const host = new URL(tab.url).host;
                const list = tabsByHost.get(host);
                if (list) {
                    list.push(tab);
                }
                else {
                    tabsByHost.set(host, [tab]);
                }
            });
            return Array.from(tabsByHost.entries())
                .map((entry) => {
                return {
                    countTotal: entry[1].length,
                    name: entry[0],
                    countHidden: entry[1].filter((tab) => tab.hidden == true).length,
                };
            })
                .sort((a, b) => {
                if (a.name.toLowerCase() < b.name.toLowerCase())
                    return -1;
                if (a.name.toLowerCase() > b.name.toLowerCase())
                    return 1;
                return 0;
            });
        });
    }
    getCountByHost(host) {
        return __awaiter(this, void 0, void 0, function* () {
            const tabs = yield this.getTabsByHost(host);
            return {
                name: host,
                countHidden: tabs.filter((tab) => tab.hidden == true).length,
                countTotal: tabs.length,
            };
        });
    }
}
var TabsManager$1 = new TabsManager();

function renderPopup(window) {
    return __awaiter(this, void 0, void 0, function* () {
        const body = createElement("body", { className: "popup" });
        const count = yield TabsManager$1.getCount();
        body.append(createLabeledRow("tabs", "", count.tabs));
        body.append(createLabeledRow("pinned tabs", "", count.pinned));
        body.append(createSeperator());
        (yield TabsManager$1.getCountsPerHost()).forEach((entry) => body.append(createSiteListRow(entry, window)));
        window.document.body.replaceWith(body);
    });
}
function createSiteListRow(entry, window) {
    let element;
    const showButton = createRowButton("show");
    showButton.addEventListener("click", (e) => __awaiter(this, void 0, void 0, function* () {
        yield TabsManager$1.showHost(entry.name);
        if (e.ctrlKey) {
            yield TabsManager$1.openHostInNewWindow(entry.name);
        }
        element.replaceWith(createSiteListRow(yield TabsManager$1.getCountByHost(entry.name), window));
    }));
    const hideButton = createRowButton("hide");
    hideButton.addEventListener("click", () => __awaiter(this, void 0, void 0, function* () {
        yield TabsManager$1.hideHost(entry.name);
        element.replaceWith(createSiteListRow(yield TabsManager$1.getCountByHost(entry.name), window));
    }));
    const deleteButton = createRowButton("delete");
    deleteButton.addEventListener("click", (e) => {
        if (e.ctrlKey) {
            TabsManager$1.closeHost(entry.name);
            element.remove();
        }
        else if (window.confirm("Are you sure?")) {
            TabsManager$1.closeHost(entry.name);
            element.remove();
        }
    });
    if (entry.name == "" ? true : false) {
        element = createLabeledRow("new tab", entry.countHidden == 0 ? "" : entry.countHidden, entry.countTotal, []);
    }
    else {
        element = createLabeledRow(entry.name, entry.countHidden == 0 ? "" : entry.countHidden, entry.countTotal, [showButton, hideButton, deleteButton]);
    }
    return element;
}
function createSeperator() {
    return createElement("hr");
}
function createLabeledRow(label, value1, value2, elements = []) {
    return createElement("div", {
        className: "popup-row",
        children: [
            createElement("span", {
                className: "popup-row__label",
                attributes: {
                    innerText: label,
                },
            }),
            createElement("div", {
                className: "popup-row__options",
                children: [createPopupValue(value1, "hidden"), createPopupValue(value2), ...elements],
            }),
        ],
    });
}
function createPopupValue(value, modifier = "") {
    return createElement("span", {
        className: `popup-row__value ${modifier == "" ? "" : "popup-row__value--" + modifier}`,
        attributes: {
            innerText: value.toString(),
        },
    });
}
function createRowButton(type) {
    const iconMap = {
        show: "externalLink",
        hide: "collection",
        delete: "trash",
    };
    return createElement("button", {
        attributes: {
            title: type,
        },
        children: [createIcon(iconMap[type])],
        className: "popup-row__button",
    });
}

function main() {
    browser.runtime.onMessage.addListener((message) => {
        if (message == "render") {
            browser.extension.getViews({ type: "popup" }).forEach((popup) => {
                renderPopup(popup);
            });
        }
    });
    browser.commands.onCommand.addListener((command) => {
        if (command == "next-tab-of-type") {
            TabsManager$1.navigateTabs(1);
        }
        if (command == "prev-tab-of-type") {
            TabsManager$1.navigateTabs(-1);
        }
    });
}
main();
